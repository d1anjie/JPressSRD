package io.jpress.addon.sdr.service.provider;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jpress.addon.sdr.service.JpressAddonSdrService;
import io.jpress.addon.sdr.model.JpressAddonSdr;
import io.jboot.service.JbootServiceBase;
import io.jpress.service.UserService;

import java.util.List;

@Bean
public class JpressAddonSdrServiceProvider extends JbootServiceBase<JpressAddonSdr> implements JpressAddonSdrService {

    @Inject
    UserService userService;

    @Override
    public Page<JpressAddonSdr> paginate(int page, int pageSize) {
        Page<JpressAddonSdr> paginate = super.paginate(page, pageSize);
        List<JpressAddonSdr> list = paginate.getList();
        for(JpressAddonSdr jpressAddonSdr:list){
            Integer userId = jpressAddonSdr.getUserId();
            jpressAddonSdr.setUser(userService.findById(userId));
        }
        return paginate;
    }
}
