package io.jpress.addon.sdr.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.validate.EmptyValidate;
import io.jboot.web.validate.Form;
import io.jpress.addon.sdr.model.JpressAddonSdr;
import io.jpress.addon.sdr.service.JpressAddonSdrService;
import io.jpress.core.menu.annotation.AdminMenu;
import io.jpress.model.User;
import io.jpress.web.base.AdminControllerBase;


@RequestMapping(value = "/admin/sdr/jpress_addon_sdr", viewPath = "/")
public class _JpressAddonSdrController extends AdminControllerBase {

    @Inject
    private JpressAddonSdrService service;

    @AdminMenu(text = "管理", groupId = "sdr")
    public void index() {
        Page<JpressAddonSdr> entries=service.paginate(getPagePara(), 10);
        setAttr("page", entries);
        render("views/jpress_addon_sdr_list.html");
    }


    public void edit() {
        int entryId = getParaToInt(0, 0);
        JpressAddonSdr entry = entryId > 0 ? service.findById(entryId) : null;
        setAttr("jpressAddonSdr", entry);
        render("views/jpress_addon_sdr_edit.html");
    }

    public void doSave() {
        JpressAddonSdr entry = getModel(JpressAddonSdr.class,"jpressAddonSdr");
        User loginedUser = getLoginedUser();
        entry.setUserId(loginedUser.getId().intValue());
        service.saveOrUpdate(entry);
        renderJson(Ret.ok().set("id", entry.getId()));
    }

    public void doDel() {
        Long id = getIdPara();
        render(service.deleteById(id) ? Ret.ok() : Ret.fail());
    }

    @EmptyValidate(@Form(name = "ids"))
    public void doDelByIds() {
        service.batchDeleteByIds(getParaSet("ids").toArray());
        renderOkJson();
    }
}
