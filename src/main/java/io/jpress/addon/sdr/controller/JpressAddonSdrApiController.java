package io.jpress.addon.sdr.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.addon.sdr.model.JpressAddonSdr;
import io.jpress.addon.sdr.service.JpressAddonSdrService;
import io.jpress.model.User;
import io.jpress.web.base.AdminControllerBase;

/**
 * 用户提交SDR和故障失效缺陷报告
 */
@RequestMapping(value = "/sdrapi",viewPath = "/")
public class JpressAddonSdrApiController extends AdminControllerBase {

    @Inject
    private JpressAddonSdrService service;

    public void saveData() {
        JpressAddonSdr entry = getModel(JpressAddonSdr.class,"jpressAddonSdr");
        User loginedUser = getLoginedUser();
        entry.setUserId(loginedUser.getId().intValue());
        service.save(entry);
        renderJson(Ret.ok().set("id", entry.getId()));
    }

}
