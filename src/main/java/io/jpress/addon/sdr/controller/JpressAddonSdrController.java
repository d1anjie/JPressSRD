package io.jpress.addon.sdr.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.addon.sdr.model.JpressAddonSdr;
import io.jpress.addon.sdr.service.JpressAddonSdrService;
import io.jpress.model.User;
import io.jpress.web.base.AdminControllerBase;
import io.jpress.web.base.TemplateControllerBase;

import java.util.Set;

/**
 * 用户提交SDR和故障失效缺陷报告
 */
@RequestMapping(value = "/sdr")
public class JpressAddonSdrController extends TemplateControllerBase {

    @Inject
    private JpressAddonSdrService service;

    public void index(){

        String language = getPara("language");
        if(language!=null && language.equals("ch")) {
            render("sdr.html");
        }else{
            render("sdr_en.html");
        }
    }

}
