package io.jpress.addon.sdr.model;

import io.jboot.db.annotation.Table;
import io.jpress.addon.sdr.model.base.BaseJpressAddonSdr;
import io.jpress.model.User;

/**
 * Generated by JPress.
 */
@Table(tableName = "jpress_addon_sdr", primaryKey = "id")
public class JpressAddonSdr extends BaseJpressAddonSdr<JpressAddonSdr> {

    private static final long serialVersionUID = 1L;
    //用户
	private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
