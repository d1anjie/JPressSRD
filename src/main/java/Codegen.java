import io.jpress.codegen.AddonGenerator;

public class Codegen {

    private static String dbUrl = "jdbc:mysql://127.0.0.1:3306/jpress";
    private static String dbUser = "root";
    private static String dbPassword = "anjie7410";

    private static String addonName = "sdr";
    private static String dbTables = "jpress_addon_sdr";
    private static String modelPackage = "io.jpress.addon.sdr.model";
    private static String servicePackage = "io.jpress.addon.sdr.service";


    public static void main(String[] args) {

        AddonGenerator moduleGenerator = new AddonGenerator(addonName, dbUrl, dbUser, dbPassword, dbTables, modelPackage, servicePackage);
        moduleGenerator.setGenUI(true);
        moduleGenerator.gen();

    }

}
